import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PainelPesquisaComponent } from './painel-pesquisa/painel-pesquisa.component';
import { PainelVisualizadorComponent } from './painel-visualizador/painel-visualizador.component';
import { PainelControleComponent } from './painel-controle/painel-controle.component';

@NgModule({
  declarations: [
    AppComponent,
    PainelPesquisaComponent,
    PainelVisualizadorComponent,
    PainelControleComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
