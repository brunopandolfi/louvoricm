import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-painel-pesquisa',
  templateUrl: './painel-pesquisa.component.html',
  styleUrls: ['./painel-pesquisa.component.css']
})
export class PainelPesquisaComponent implements OnInit {

  @ViewChild ('listbox') listbox: ElementRef;
  private select: any;
  listaItens: any[] = ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6'];
  constructor() {

  }

  ngOnInit() {
    this.listbox.nativeElement.style.height = window.innerHeight * 0.8 + 'px';
    this.listbox.nativeElement.style.backgroundColor = 'white';
  }

  selecionarItem (event, novoItem)
  {
    console.log(novoItem);
    this.select = novoItem;
  }

}
