import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelPesquisaComponent } from './painel-pesquisa.component';

describe('PainelPesquisaComponent', () => {
  let component: PainelPesquisaComponent;
  let fixture: ComponentFixture<PainelPesquisaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PainelPesquisaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelPesquisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
