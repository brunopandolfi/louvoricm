import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelVisualizadorComponent } from './painel-visualizador.component';

describe('PainelVisualizadorComponent', () => {
  let component: PainelVisualizadorComponent;
  let fixture: ComponentFixture<PainelVisualizadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PainelVisualizadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelVisualizadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
