import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-painel-visualizador',
  templateUrl: './painel-visualizador.component.html',
  styleUrls: ['./painel-visualizador.component.css']
})
export class PainelVisualizadorComponent implements OnInit {

  @ViewChild ('visualizador') visualizador: ElementRef;
  @ViewChild ('titulo-tela') titulo_tela: ElementRef;
  @ViewChild ('texto') texto: ElementRef;

  constructor() { }

  ngOnInit() {
    this.visualizador.nativeElement.style.width = '60%';
    this.visualizador.nativeElement.style.height = window.innerHeight * 0.70 + 'px';
    //this.visualizador.nativeElement.style.width = window.innerWidth + 'px';
    this.visualizador.nativeElement.style.marginTop = '0';
    this.visualizador.nativeElement.style.marginLeft = '15px';
    this.visualizador.nativeElement.style.background = 'linear-gradient(to bottom, #000066 0%, #003399 69%)';
    this.texto.nativeElement.style.fontSize = '28pt';
    this.texto.nativeElement.style.marginTop = '100px';
  }
}
